<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    protected $fillable = [
        'user', 'post',
    ];

    function addPost($user, $content) {
        DB::table('posts')->insert(
            [ 'user' => $user, 'post' => $content]
        );
    }
}
